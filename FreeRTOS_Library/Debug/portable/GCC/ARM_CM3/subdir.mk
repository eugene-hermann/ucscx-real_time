################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../portable/GCC/ARM_CM3/port.c 

OBJS += \
./portable/GCC/ARM_CM3/port.o 

C_DEPS += \
./portable/GCC/ARM_CM3/port.d 


# Each subdirectory must supply rules for building sources it contributes
portable/GCC/ARM_CM3/%.o: ../portable/GCC/ARM_CM3/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -I"/home/eug/proj/ucsc_e/real-time/final-project/FreeRTOS_Library/include" -I"/home/eug/proj/ucsc_e/real-time/final-project/CMSISv1p30_LPC17xx/inc" -I"/home/eug/proj/ucsc_e/real-time/final-project/FreeRTOS_Library/portable" -I"/home/eug/proj/ucsc_e/real-time/final-project/FreeRTOS_Library/demo_code" -O1 -g3 -Wall -c -fmessage-length=0 -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


