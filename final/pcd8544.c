/*
  PCD8544 controller limited driver for LPC1769 board.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include "conf.h"

#include "LPC17xx.h"

#include "pcd8544.h"

#if PCD_8544_SSP_EMULATION
void pcd8544_init_gpio_emu() {
  // set the direction of the pins
  PCD_8544_ENABLE_GPIO_PINS_emu();
  PCD_8544_SET_OUTPUT_PINS_emu();
  // set the default state for the pins
  PCD_8544_PORT->FIOSET = PCD_8544_ALL_PINS;
}

static void pcd8544_put_data_emu(uint8_t data) {
  // enable LCD
  PCD_8544_PORT->FIOCLR = PCD_8544_CS;

  uint8_t i;
  for(i = 0; i < 8; i++) {
    // SPI_MO = data & 0x80;
    if (data & 0x80) {
      PCD_8544_PORT->FIOSET = PCD_8544_DATA;
    } else {
      PCD_8544_PORT->FIOCLR = PCD_8544_DATA;
    }
    data = data << 1;
    // toggle the clock pin
    PCD_8544_PORT->FIOCLR = PCD_8544_CLK;
    __asm("nop;");   // Not sure if I need it, but I feel safe when I have it.
    PCD_8544_PORT->FIOSET = PCD_8544_CLK;

  }
  // disable LCD
  PCD_8544_PORT->FIOSET = PCD_8544_CS;
}

#else

// Table 371  SSPn Control Register 0
#define SSP_CR0_DSSx        0x07         // 8-bit Data Size Select.
#define SSP_CR0_FRF        (0x00 << 4)   // SPI Frame Format
#define SSP_CR0_CPOL       (0x00 << 6)   // SSP controller maintains the bus clock low between frames
#define SSP_CR0_CPHA       (0x00 << 7)   // SSP controller captures serial data on the first clock transition of the frame
#define SSP_CR0_SCR(rate)  (rate << 8)   // Serial Clock Rate.

// Table 372 SSPn Control Register 1
#define SSP_CR1_SSE (1 << 1) // SSP Enable
#define SSP_CR1_MS  (1 << 2) // Master Mode

#define SSP_CLK_DIV 4
#define SSP_CLOCK_VALUE(base_clock, cr0, prescale) (base_clock / ((cr0 + 1) * prescale))
extern uint32_t SystemCoreClock;

// Clock divider is 4 => corresponding PCLKSEL bits value is 00
// Set Clock Prescale Register and return value for Control Register SCR bits
__inline static
uint32_t sspSetClockPrescale1(uint32_t freq) {
  uint32_t prescale, cr0_scr, current_clock;
  uint32_t ssp_clock = SystemCoreClock / SSP_CLK_DIV;
  cr0_scr = 0;
  prescale = 2;
  current_clock = SSP_CLOCK_VALUE(ssp_clock, cr0_scr, prescale);
  while (current_clock > freq) {
    cr0_scr++;
    if (cr0_scr > 0xFF) {
      cr0_scr = 0;
      prescale += 2;
    }
    current_clock = SSP_CLOCK_VALUE(ssp_clock, cr0_scr, prescale);
  }
  PCD_8544_BUS->CPSR = prescale;
  return cr0_scr;
}

void pcd8544_init_bus_ssp() {
  // p421
  LPC_SC->PCONP |= PCONP_PCSSP0;
  PCD_8544_BUS->CR1 = 0;
  uint32_t scr = sspSetClockPrescale1(10000000);
  PCD_8544_BUS->CR0 = SSP_CR0_DSSx | SSP_CR0_FRF | SSP_CR0_CPOL | SSP_CR0_CPHA | SSP_CR0_SCR(scr);
  PCD_8544_BUS->CR1 = SSP_CR1_SSE | SSP_CR1_SSE;
}

// Common pcd8544 protocol logic.
// Abstraction level that must work with any hardware port.
#endif //PCD_8544_SSP_EMULATION


void pcd8544_command(uint8_t cmd) {
  wait_ssp_status_not_busy();
  pcd8544_set_command_mode();
  pcd8544_put_data(cmd);
  wait_ssp_status_not_busy();
}

void pcd8544_write_data(uint8_t data) {
  wait_ssp_status_tx_not_full();
  pcd8544_set_data_mode();
  pcd8544_put_data(data);
}

void pcd8544_reset() {
  pcd8544_rst_low();
  wait_at_least_100_ns();
  pcd8544_rst_high();
}

