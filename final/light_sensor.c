/*
  ADC handler for photo cell.
  Real-time Embedded Systems Programming. Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include "LPC17xx.h"

#include "conf.h"

#define BIT_14_15_ZERO (~(3 << 14))
#define BIT_14_ONE     (1 << 14)
#define BIT_15_ONE     (1 << 15)

#define ADC_DONE (1 << 31)
#define ADC_OVERRUN (1 << 30)


#define ADC_START() DO(LPC_ADC->ADCR |= (1 << 24))
#define ADC_STOP() DO(LPC_ADC->ADCR &= ~(0x07 << 24))

void lsensor_setup() {
  const uint32_t ADC_clock = 1000000;
  //  A/D Converter is powered on
  LPC_SC->PCONP |= 1 << 12;

  //  set P0.23 as AD0.0
  LPC_PINCON->PINSEL1 &= BIT_14_15_ZERO;
  LPC_PINCON->PINSEL1 |= BIT_14_ONE;

  //  Set PINMODE of AD0.0 as no pull-up/down resistors
  LPC_PINCON->PINMODE1 &= BIT_14_15_ZERO;
  LPC_PINCON->PINMODE1 |= BIT_15_ONE;

  uint32_t clock_bit = ((SystemCoreClock / ADC_clock - 1 ) << 8);
  //  select AD0.0, CLKDIV, power on
  LPC_ADC->ADCR  = 1 | clock_bit | (1 << 21);
}

uint32_t lsensor_get_value() {
  uint32_t volatile regVal = 0;

  ADC_START();
  while ( (regVal & ADC_DONE) == 0 ) {
    regVal = LPC_ADC->ADGDR;
  }
  regVal = LPC_ADC->ADDR0;
  // Step 5: stop A/D conversion for now
  ADC_STOP();

  // Check if there was an overrun and if lost any data
  if ( regVal & ADC_OVERRUN ) {
    return 0;
  }
  return (regVal >> 4) & 0x0fff;
}
