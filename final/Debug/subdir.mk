################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cr_startup_lpc17.c \
../hooks.c \
../light_sensor.c \
../main.c \
../nokia5510.c \
../pcd8544.c \
../push_button.c \
../usonic.c 

OBJS += \
./cr_startup_lpc17.o \
./hooks.o \
./light_sensor.o \
./main.o \
./nokia5510.o \
./pcd8544.o \
./push_button.o \
./usonic.o 

C_DEPS += \
./cr_startup_lpc17.d \
./hooks.d \
./light_sensor.d \
./main.d \
./nokia5510.d \
./pcd8544.d \
./push_button.d \
./usonic.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"/home/eug/proj/ucsc_e/real-time/final-project/FreeRTOS_Library/demo_code" -I"/home/eug/proj/ucsc_e/real-time/final-project/CMSISv1p30_LPC17xx/inc" -I"/home/eug/proj/ucsc_e/real-time/final-project/FreeRTOS_Library/include" -I"/home/eug/proj/ucsc_e/real-time/final-project/FreeRTOS_Library/portable" -O1 -g3 -fsigned-char -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


