/*
  Nokia 5510 LCD driver for LPC1769 board.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include "conf.h"

#include "LPC17xx.h"

#include "pcd8544.h"

#include "nokia5510_font.h"
#include "nokia5510.h"

// LED Brightness PWM control. LPC17xx only.
#define NOKIA5510_BR_PORT  LPC_GPIO2
#define NOKIA5510_BR_PIN   BIT_0_ONE
//    The PWM is configured using the following registers:
//    1. Power: In the PCONP register (Table 46), set bit PCPWM1.
//    Remark: On reset, the PWM is enabled (PCPWM1 = 1).
//    2. Peripheral clock: In the PCLKSEL0 register (Table 40), select PCLK_PWM1.
//    3. Pins: Select PWM pins through the PINSEL registers. Select pin modes for port pins
//    with PWM1 functions through the PINMODE registers (Section 8.5).
//    4. Interrupts: See registers PWM1MCR (Table 450) and PWM1CCR (Table 451) for
//    match and capture events. Interrupts are enabled in the NVIC using the appropriate
//    Interrupt Set Enable register.
#define PCPWM1       BIT_6_ONE
#define LER0_EN      BIT_0_ONE
#define LER1_EN      BIT_1_ONE

#define PWM1_PCR_EN  BIT_9_ONE
#define TCR_CNT_EN 0x00000001
#define TCR_PWM_EN 0x00000008
#define END_PWM_VAL 25500

static __inline void nokia5510_pwm_init() {
  //enable PWM1 Power
  LPC_SC->PCON |= PCPWM1;
  //PWM peripheral clk = PCLK
  LPC_SC->PCLKSEL0 &= BITS_12_13_ZERO;
  //Pin select
  LPC_PINCON->PINSEL4 |= (0x1<<0);
  // count frequency:Fpclk
  LPC_PWM1->PR = 0x00;
  //reset on MR0
  LPC_PWM1->MCR = 1 << 1;
  // set PWM cycle
  LPC_PWM1->MR0 = END_PWM_VAL;
  //LEDs default to OFF
  LPC_PWM1->MR1 = 0;

  //Load Shadow register content
  LPC_PWM1->LER = LER0_EN | LER1_EN;
  //Enable PWM outputs
  LPC_PWM1->PCR = PWM1_PCR_EN;
  //Enable PWM Timer
  LPC_PWM1->TCR = TCR_CNT_EN | TCR_PWM_EN;
}

static __inline void nokia5510_pwm_set_value(uint32_t value) {
  // convert from logical value to PWM duty circle value.
  LPC_PWM1->MR1 = value * 100;
  LPC_PWM1->LER = LER0_EN | LER1_EN;
  //DBG_PRINTF("MR1 = %u",  LPC_PWM1->MR1);
  // TODO: For power saving disable PWM if value = 0 or END_PWM_VAL
}


// Nokia 5510 display specific.

#define LCD_MAX_X 14
#define LCD_MAX_Y 6

typedef struct {
  uint8_t  x;
  uint8_t  y;
  uint32_t led_brightness;
} LCD_context;

LCD_context gLCD_ctx = {0, 0, 0};

void nokia5510_init() {
  pcd8544_init_gpio();
  pcd8544_init_bus();
  pcd8544_reset();

  pcd8544_command(0x21);
  pcd8544_command(0xc6);
  pcd8544_command(0x06);
  pcd8544_command(0x13);
  pcd8544_command(0x20);
  nokia5510_clear();
  pcd8544_command(0x0c);

  nokia5510_pwm_init();
}

void nokia5510_clear() {
  uint8_t i,j;
  for(i = 0; i < 6; i++) {
    for(j = 0; j < 84; j++) {
      pcd8544_write_data(0);
    }
  }
  nokia5510_setcursor(0, 0);
}


void nokia5510_setcursor(uint8_t x, uint8_t y) {
  // X increments in pixels
  pcd8544_command(0x80 | x * 7);
  y %= LCD_MAX_Y;
  // Y increments in rows of 8 pixels
  pcd8544_command(0x40 | y);
  gLCD_ctx.x = x;
  gLCD_ctx.y = y;
}

static void nokia5510_putchar(char c, uint8_t print_mode) {
  uint8_t column;
  uint8_t nokia_byte;
  for(column = 0; column < 5; column++) {
    nokia_byte = nokia5510_ascii_byte(c, column, print_mode);
    pcd8544_write_data(nokia_byte);
  }
  // Additional zero byte is a space between letters.
  pcd8544_write_data(((print_mode) == NOKIA5510_PRINT_NORMAL) ? 0x00 : 0xff);
  gLCD_ctx.x++;
}


static void nokia5510_put_line_mode(char *line, uint8_t print_mode) {
  while(*line != '\0' && gLCD_ctx.x < LCD_MAX_X) {
    nokia5510_putchar(*line, print_mode);
    line++;
  }
  nokia5510_setcursor(0, gLCD_ctx.y + 1);
}

void nokia5510_put_line(char *line) {
  nokia5510_put_line_mode(line, NOKIA5510_PRINT_NORMAL);
}

void nokia5510_put_line_inverse(char *line) {
  nokia5510_put_line_mode(line, NOKIA5510_PRINT_INVERSE);
}

void nokia5510_set_brightness(uint32_t level) {
  if (level != gLCD_ctx.led_brightness) {
    gLCD_ctx.led_brightness = level;
    nokia5510_pwm_set_value(level);
  }
}
