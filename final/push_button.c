/*
  GPIO interrupts handler.
  Real-time Embedded Systems Programming. Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "LPC17xx.h"

#include "conf.h"
#include "gpio_isr.h"
#include "usonic.h"

//xSemaphoreHandle pbutton_semaphore = NULL;
static TaskHandle_t bottom_half = NULL;

void gpio_set_bottom_half_handler(TaskHandle_t new_bottom_half) {
  bottom_half = new_bottom_half;
}

void pbutton_setup () {

  //configure pin for gpio functionality
  LPC_PINCON->PINSEL0 &= (~(3 << (9*2)));
  //configure gpio as input (set the right bit to *zero*)
  LPC_GPIO0->FIODIR &= (~(1 << 9));
  LPC_GPIO0->FIOCLR = (1 << 9);
  //enable rising edge interrupts on gpio pin
  LPC_GPIOINT->IO0IntEnR |= (1 << 9);
  LPC_GPIOINT->IO0IntClr = UINT32_MAX;

  //enable the interrupts on gpio
  NVIC_SetPriority( EINT3_IRQn, (configMAX_SYSCALL_INTERRUPT_PRIORITY) + 1);
  NVIC_EnableIRQ(EINT3_IRQn);
}

__inline static void notify_bottom_half_FromISR(uint32_t value, portBASE_TYPE *woken) {
  if(bottom_half != NULL) {
    xTaskNotifyFromISR(bottom_half, value, eSetBits, woken);
  }
}

void EINT3_IRQHandler (void) {
  portBASE_TYPE woken = pdFALSE;
  uint32_t IO0IntStatR = LPC_GPIOINT->IO0IntStatR;
#if USONIC_GPIO_INTERRUPTS
  uint32_t IO0IntStatF = LPC_GPIOINT->IO0IntStatF;
  if (IO0IntStatR & USONIC_ECHO_BIT) {
    usonic_echo_start_FromISR();
    LPC_GPIOINT->IO0IntClr |= USONIC_ECHO_BIT;
  } else if (IO0IntStatF & USONIC_ECHO_BIT) {
    usonic_echo_end_FromISR();
    LPC_GPIOINT->IO0IntClr |= USONIC_ECHO_BIT;
  } else
#endif
  if (IO0IntStatR & (1 << 9)) {
    //raising edge interrupt on pin 0.9 was fired
    LPC_GPIOINT->IO0IntClr |= (1 << 9); // clear the status
    // 'Give' the semaphore to unblock the task.
    notify_bottom_half_FromISR(NOTIFY_BUTTON_TOUCH, &woken);
  }
  NVIC_ClearPendingIRQ(EINT3_IRQn);
  portEND_SWITCHING_ISR(woken);
}


