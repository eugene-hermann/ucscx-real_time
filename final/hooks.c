#include "FreeRTOS.h"
#include "task.h"

void vApplicationMallocFailedHook( void ) {
	/* This function will only be called if an API call to create a task, queue
	or semaphore fails because there is too little heap RAM remaining. */
	for( ;; );
}

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName ) {
	/* This function will only be called if a task overflows its stack.  Note
	that stack overflow checking does slow down the context switch
	implementation. */
	for( ;; );
}

void vApplicationIdleHook( void ) {
	/* This example does not use the idle hook to perform any processing. */
}

void vApplicationTickHook( void ) {
	/* This example does not use the tick hook to perform any processing. */
}
