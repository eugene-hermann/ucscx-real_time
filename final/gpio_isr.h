/*
  GPIO handler.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#ifndef __GPIO_ISR__INCLUDED__
#define __GPIO_ISR__INCLUDED__
#include "FreeRTOS.h"
#include "task.h"

#include "conf.h"

void gpio_set_bottom_half_handler(TaskHandle_t new_bottom_half);

void pbutton_setup();

#endif
