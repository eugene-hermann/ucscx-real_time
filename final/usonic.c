/*
  HC-SR04 ultrasonic sonar driver for LPC1769 board.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

// Two working configurations:
// 1. Timer interrupts, works buy capturing timestamps of
// the echo pin raise/fall events into timer CAP register.
// can be time sensitive. Has soft deadline, but hard-wired time measurement.
// 2. Simple Arduino-style busy loop, used mainly for testing purposes.

// GPIO configuration might be in broken state.
// But anyway, it was implemented only to prove, that this is wrong approach,
// not schedulable with ~1 us deadline.

#include "LPC17xx.h"
#include "FreeRTOS.h"
#include "task.h"

#include "conf.h"
#include "usonic.h"



#define usonic_timeout(__value__) (__value__ >= 100000)
#define usonic_wait(__value__) ((__value__ = USONIC_TIMER_PORT->TC) < 100000)
#define usonic_echo_high() (LPC_GPIO0->FIOPIN & USONIC_ECHO_BIT)
#define usonic_echo_low() ((LPC_GPIO0->FIOPIN & USONIC_ECHO_BIT) == 0)

__inline static void timer_stop();

#if USONIC_INTERRRUPTS
static TaskHandle_t bottom_half = NULL;

void usonic_set_bottom_half_handler(TaskHandle_t new_bottom_half) {
  bottom_half = new_bottom_half;
}
__inline static void notify_bottom_half_FromISR(uint32_t value, portBASE_TYPE *woken) {
  if(bottom_half != NULL) {
    xTaskNotifyFromISR(bottom_half, value, eSetBits, woken);
  }
}
#endif


#define USONIC_CLK_DIV 4
// Clock divider is 4 => corresponding PCLKSEL bits value is 00
// Timer clock is a system core clock divided by DIV defined in PCLKSEL
extern uint32_t SystemCoreClock;
__inline static uint32_t usecTimerPrescale() {
  uint32_t prescale = SystemCoreClock / USONIC_CLK_DIV;
  prescale /= 1000000;
  return prescale;
}


// Mask to get the Counter/timer mode bits
#define TIM_CTCR_MODE_MASK  0x3
/** Timer/counter enable bit */
#define TIM_ENABLE    BIT_0_ONE
/** Timer/counter reset bit */
#define TIM_RESET     BIT_1_ONE

#define PCONP_PCTIM0       BIT_1_ONE
#define PCONP_PCTIM3       BIT_23_ONE
#define USONIC_PCONP       PCONP_PCTIM3

// Table 40. Find Peripheral clock divider bit position
// Set PCLK_peripheral = CCLK/4
#define PCLKSEL_TIMER0_SET     LPC_SC->PCLKSEL0 &= (~(0x03 << 2))        // Set bits 3:2 of PCLKSEL0 zero
#define PCLKSEL_TIMER3_SET     DO( LPC_SC->PCLKSEL1 &= (~(0x03 << 14)) ) // Set bits 14:15 of PCLKSEL1 zero
#define USONIC_CLOCK_DIV_SET() PCLKSEL_TIMER3_SET

#if USONIC_TIMER_INTERRUPTS

// UM10360.pdf, p505. table 430
#define MR0I    BIT_0_ONE
#define MR0R    BIT_1_ONE
#define MR0S    BIT_2_ONE
#define CAP1RE  BIT_3_ONE
#define CAP1FE  BIT_4_ONE
#define CAP1I   BIT_5_ONE


void usonic_setup() {
  LPC_PINCON->PINSEL0 &= PINSEL0_USONIC_MASK;
  LPC_PINCON->PINSEL1 &= PINSEL1_USONIC_MASK; // Reset PINSEL1 usonic bits.

  LPC_GPIO0->FIODIR &= USONIC_ECHO_LOW;       // Set Echo pint to GPIO input.
  LPC_GPIO0->FIODIR |= USONIC_TRIG_BIT;
  LPC_GPIO0->FIOCLR = USONIC_ECHO_BIT | USONIC_TRIG_BIT;

  LPC_GPIOINT->IO0IntEnR &= USONIC_ECHO_LOW;
  LPC_GPIOINT->IO0IntEnF &= USONIC_ECHO_LOW;

  LPC_PINCON->PINSEL1 |= PINSEL1_USONIC_SET;  // Set PINSEL1 to alternate function for timer capture on echo pin.
  NVIC_ClearPendingIRQ(TIMER3_IRQn);
  NVIC_SetPriority(TIMER3_IRQn, (configMAX_SYSCALL_INTERRUPT_PRIORITY) + 1);
  NVIC_DisableIRQ(TIMER3_IRQn);
}


__inline static void timer_start() {
  LPC_SC->PCONP |= USONIC_PCONP;  // Power on
  // set divider CLKPWR_PCLKSEL_CCLK_DIV_4
  USONIC_CLOCK_DIV_SET();

  USONIC_TIMER_PORT->TC = 0;
  USONIC_TIMER_PORT->PC = 0;
  USONIC_TIMER_PORT->PR = usecTimerPrescale() - 1;

  // Configure timer timeout interrupt.
  // Required with any of async modes.
  USONIC_TIMER_PORT->MR0 = 50000;                      // Match value 50 msec for timeout
  USONIC_TIMER_PORT->MCR = (MR0I /*| MR0S*/);
  USONIC_TIMER_PORT->CCR = (CAP1RE /*| CAP1FE*/ | CAP1I);  // Capture events on Echo pin

  USONIC_TIMER_PORT->TCR |= (TIM_RESET | TIM_ENABLE);  // Reset Counter
  USONIC_TIMER_PORT->TCR &= ~(TIM_RESET);              // release reset
  USONIC_TIMER_PORT->IR = UINT32_MAX;
  NVIC_EnableIRQ(TIMER3_IRQn);
}

#else

void usonic_setup() {
  LPC_PINCON->PINSEL0 &= PINSEL0_USONIC_MASK;
  LPC_PINCON->PINSEL1 &= PINSEL1_USONIC_MASK; // Reset PINSEL1 usonic bits.

  LPC_GPIO0->FIOCLR = USONIC_ECHO_BIT | USONIC_TRIG_BIT;
  LPC_GPIO0->FIODIR &= USONIC_ECHO_LOW;       // Set Echo pint to GPIO input.
  LPC_GPIO0->FIODIR |= USONIC_TRIG_BIT;

#if USONIC_GPIO_INTERRUPTS
  LPC_GPIOINT->IO0IntEnR |= USONIC_ECHO_BIT;
  LPC_GPIOINT->IO0IntEnF |= USONIC_ECHO_BIT;
#else
  LPC_GPIOINT->IO0IntEnR &= USONIC_ECHO_LOW;
  LPC_GPIOINT->IO0IntEnF &= USONIC_ECHO_LOW;
#endif // USONIC_GPIO_INTERRUPTS
}


__inline static void timer_start() {
  // lpc17xx_timer  TIM_Init
  LPC_SC->PCONP |= USONIC_PCONP;  // Power on
  // set divider CLKPWR_PCLKSEL_CCLK_DIV_4
  USONIC_CLOCK_DIV_SET();

  USONIC_TIMER_PORT->CCR &= ~TIM_CTCR_MODE_MASK;
  USONIC_TIMER_PORT->TC = 0;
  USONIC_TIMER_PORT->PC = 0;
  USONIC_TIMER_PORT->PR = usecTimerPrescale() - 1;

//  USONIC_TIMER_PORT->MCR &= ~0x03;
#if USONIC_INTERRRUPTS
  NVIC_EnableIRQ(TIMER3_IRQn);
  // Configure timer timeout interrupt.
  // Required with any of async modes.
  USONIC_TIMER_PORT->MR0 = 500000;                      // Match value 10 msec for timeout
  USONIC_TIMER_PORT->MCR = (MR0I | MR0S);
#endif
#if USONIC_TIMER_INTERRUPTS
  USONIC_TIMER_PORT->CCR = (CAP1RE | CAP1FE | CAP1I);  // Capture events on Echo pin
#endif

  USONIC_TIMER_PORT->TCR |= (TIM_RESET | TIM_ENABLE);  // Reset Counter
  USONIC_TIMER_PORT->TCR &= ~(TIM_RESET);              // release reset
  USONIC_TIMER_PORT->IR = 0x0;
}

#endif


#if USONIC_GPIO_INTERRUPTS
__inline static void timer_start_gpio() {
  // lpc17xx_timer  TIM_Init
  LPC_SC->PCONP |= USONIC_PCONP;  // Power on
  // set divider CLKPWR_PCLKSEL_CCLK_DIV_4
  //LPC_SC->PCLKSEL0 &= (~(0x03 << PCLKSEL_TIMER0));
  USONIC_CLOCK_DIV_SET();
  USONIC_TIMER_PORT->CCR &= ~TIM_CTCR_MODE_MASK;
  USONIC_TIMER_PORT->TC = 0;
  USONIC_TIMER_PORT->PC = 0;
  USONIC_TIMER_PORT->PR = usecTimerPrescale() - 1;

  USONIC_TIMER_PORT->TCR |= (TIM_RESET | TIM_ENABLE); //Reset Counter
  USONIC_TIMER_PORT->TCR &= ~(TIM_RESET); //release reset
}
#endif

__inline static void timer_stop() {
  NVIC_DisableIRQ(TIMER3_IRQn);
  USONIC_TIMER_PORT->TCR = 0x00;    // Stop counter.
  LPC_SC->PCONP &= (~USONIC_PCONP); // Power off
}

__inline static void triger_usonic() {
  timer_start();
  LPC_GPIO0->FIOSET = USONIC_TRIG_BIT;
  while (USONIC_TIMER_PORT->TC < 10) { }
  LPC_GPIO0->FIOCLR = USONIC_TRIG_BIT;
}


int usonic_get_value() {
  volatile uint32_t startValue = 0;
  volatile uint32_t endValue = 0;
  triger_usonic();
  // startValue = USONIC_TIMER_PORT->TC after this line;
  while (usonic_wait(startValue) && usonic_echo_low()) { }
  // endValue = USONIC_TIMER_PORT->TC after this line;
  while (usonic_wait(endValue) && usonic_echo_high()) { }

  timer_stop();
  DBG_PRINTF("%u - %u = %u(us)", endValue, startValue, endValue - startValue);
  if (usonic_timeout(endValue)) {
    return -1;
  }
  int mm = usonic_mm_from_us(endValue - startValue);
  return mm;
}

#if USONIC_INTERRRUPTS
static uint32_t start = 0;
static uint32_t end = 0;

void usonic_value_triger_async() {
  start = 0;
  end = 0;
  triger_usonic();
}


#if USONIC_GPIO_INTERRUPTS
void usonic_echo_start_FromISR() {
  start = USONIC_TIMER_PORT->TC;
}

void usonic_echo_end_FromISR() {
  end = USONIC_TIMER_PORT->TC;
  timer0_stop();
  DBG_PRINTF("%u - %u = %u(us)", end, start, end - start);
}
#endif //USONIC_GPIO_INTERRUPTS

#define TIMEOUT_Int BIT_0_ONE // Timeout value stored in MR0.
#define ECHO_Int    BIT_5_ONE // CR1 Raise/Fall is echo start/stop
void TIMER3_IRQHandler() {
  portBASE_TYPE woken = pdFALSE;
  volatile uint32_t IR = USONIC_TIMER_PORT->IR;
  volatile uint32_t PIN = (LPC_GPIO0->FIOPIN >> USONIC_ECHO_PIN) & 1;

  USONIC_TIMER_PORT->IR = UINT32_MAX;
  if (IR & ECHO_Int) {
    if (PIN) {
      start = USONIC_TIMER_PORT->CR1;
      USONIC_TIMER_PORT->CCR = (CAP1FE | CAP1I);
    } else {
      uint32_t value;
      end = USONIC_TIMER_PORT->CR1;
      timer_stop();
      value = (end - start) & NOTIFY_VALUE_MASK;
      DBG_PRINTF("%u us", end - start);
      notify_bottom_half_FromISR(value | NOTIFY_USONIC_VALUE, &woken);
    }
  } else   if (IR & TIMEOUT_Int) {
    start = end = 0;
    DBG_PRINTF("timeout %u", USONIC_TIMER_PORT->TC);
    timer_stop();
    notify_bottom_half_FromISR(NOTIFY_USONIC_ERROR, &woken);
  }

  portEND_SWITCHING_ISR(woken);
}

#endif

#if TEST_USONIC
// Bare-metal test for usonic HCSR-04
void test_usonic() {
  volatile uint32_t startValue = 0;
  volatile uint32_t endValue = 0;
  for (;;) {
    printf("trig start\n");
    triger_usonic();
    while (usonic_echo_low()) { }
    startValue = USONIC_TIMER_PORT->TC;
    while (usonic_echo_high()) { }
    endValue = USONIC_TIMER_PORT->TC;
    long int mm = ((endValue - startValue) * 5) / 291;
    printf("Echo end %u-%u=%u us, %li mm\n", endValue, startValue, (endValue - startValue), mm);
    while (USONIC_TIMER_PORT->TC < USONIC_TIMER_PORT->MR0 - 1) { }
    timer_stop();
  }
}
#endif //TEST_USONIC


