/*
  Nokia 5510 LCD driver for LPC1769 board.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#ifndef NOKIA5510_DRV_INCLUDED
#define NOKIA5510_DRV_INCLUDED

void nokia5510_init();

void nokia5510_clear();

void nokia5510_setcursor(uint8_t x, uint8_t y);

// Print string on LCD.
// string - Null-terminated ACII-string, max length = char[14] + '\0', the rest will be right trimmed.
// Acceptable characters from 0x20(' ') to 0x7e('~'), otherwise meet a HardFalut, no char validation inside.
// Does CR after line.
void nokia5510_put_line(char *string);

// Same as nokia5510_put_line, but negative pixel logic for characters.
void nokia5510_put_line_inverse(char *string);

// set LED backlight brightness.
// level from 0 to 255
void nokia5510_set_brightness(uint32_t level);

#endif // NOKIA5510_DRV_INCLUDED
