/*
  PCD8544 controller limited driver for LPC1769 board.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#ifndef __PCD8544_INCLUDED__
#define __PCD8544_INCLUDED__

// PCD8544 port for LPC17xx
#define PCD_8544

#define PCD_8544_PORT     LPC_GPIO0
// port 0.5 is MODE pin
#define PCD_8544_DC_PIN   BIT_5_ONE
// port 0.6 is RESET pin
#define PCD_8544_RST_PIN  BIT_6_ONE


// At 120 MHz 1 circle is 8.33ns= > 12 circles delay is about 100ns
#define wait_at_least_100_ns()   { \
  volatile int delay; \
  for (delay = 0; delay < 13; delay++) {} \
}

#define pcd8544_set_data_mode()       DO(PCD_8544_PORT->FIOSET = PCD_8544_DC_PIN)
#define pcd8544_set_command_mode()    DO(PCD_8544_PORT->FIOCLR = PCD_8544_DC_PIN)

#define pcd8544_rst_low()             DO(PCD_8544_PORT->FIOCLR = PCD_8544_RST_PIN)
#define pcd8544_rst_high()            DO(PCD_8544_PORT->FIOSET = PCD_8544_RST_PIN)


#if PCD_8544_SSP_EMULATION

#define PCD_8544_CS   (1 << 16)      // port 0.16 is CS pin
#define PCD_8544_CLK  (1 << 15)      // port 0.15 is CLOCK pin
#define PCD_8544_DATA (1 << 18)      // port 0.18 is DATA pin


#define PCD_8544_ENABLE_GPIO_PINS_emu() DO (\
  LPC_PINCON->PINSEL0 &= (~((0x03 << 30) | (0x03 << 10) | (0x03 << 12))); \
  LPC_PINCON->PINSEL1 &= (~((0x03 << 0) | (0x03 << 4))); \
)

#define PCD_8544_ALL_PINS              (PCD_8544_CS | PCD_8544_CLK | PCD_8544_DATA | PCD_8544_RST_PIN | PCD_8544_DC_PIN)
#define PCD_8544_SET_OUTPUT_PINS_emu() DO(LPC_GPIO0->FIODIR |= PCD_8544_ALL_PINS)

#define wait_ssp_status_not_busy()
#define wait_ssp_status_tx_not_full()
#define pcd8544_init_gpio             pcd8544_init_gpio_emu
#define pcd8544_init_bus()
#define pcd8544_put_data              pcd8544_put_data_emu


#else //PCD_8544_SSP_EMULATION
// SSP Status register
#define SSPSR_TFE   BIT_0_ONE
#define SSPSR_TNF   BIT_1_ONE
#define SSPSR_RNE   BIT_2_ONE
#define SSPSR_RFF   BIT_3_ONE
#define SSPSR_BSY   BIT_4_ONE

#define PCD_8544_BUS      LPC_SSP0
/** The SSP0 interface power/clock control bit */
#define  PCONP_PCSSP0 (1 << 21)


#define PCD_8544_ENABLE_GPIO_PINS_SSP() DO ( \
  LPC_PINCON->PINSEL0 &= (~((0x03 << 10) | (0x03 << 12) | (0x03 << 30) )); \
  LPC_PINCON->PINSEL0 |= (0x02 << 30); \
  LPC_PINCON->PINSEL1 &= (~( (0x03 << 4) | (0x03 << 0) )); \
  LPC_PINCON->PINSEL1 |= ( (0x02 << 4) | (0x02 << 0) ); \
)

#define PCD_8544_SET_OUTPUT_PINS_SSP() \
  DO( LPC_GPIO0->FIODIR |= (PCD_8544_DC_PIN | PCD_8544_RST_PIN) )

#if (defined(PCD_8544_PORT) && defined(PCD_8544_DC_PIN) && defined(PCD_8544_BUS))
#define wait_ssp_status_not_busy()    while ((PCD_8544_BUS->SR & SSPSR_BSY) != 0) {}
#define wait_ssp_status_tx_not_full() while ((PCD_8544_BUS->SR & SSPSR_TNF) == 0) {}
#define pcd8544_put_data_ssp(_DATA)   DO(PCD_8544_BUS->DR = ((_DATA) & 0xFFFF))

#define pcd8544_init_gpio             pcd8544_init_gpio_ssp
#define pcd8544_init_bus              pcd8544_init_bus_ssp
#define pcd8544_put_data              pcd8544_put_data_ssp



void pcd8544_init_bus_ssp();

static __inline void pcd8544_init_gpio_ssp() {
  PCD_8544_ENABLE_GPIO_PINS_SSP();
  PCD_8544_SET_OUTPUT_PINS_SSP();
}

void pcd8544_command(uint8_t cmd);
void pcd8544_write_data(uint8_t data);
void pcd8544_reset();

#else
#error Configuration unknown
#endif // Predefined configuration.
#endif //PCD_8544_SSP_EMULATION

#endif // __PCD8544_INCLUDED__
