/*
  Real-time Embedded Systems Programming. Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "conf.h"
#include "gpio_isr.h"
#include "nokia5510.h"
#include "usonic.h"

void lsensor_setup();
uint32_t lsensor_get_value();


#define SwitchStateAlwaysOn  0
#define SwitchStateAutomatic 1


#define light_is_allways_on(ctx)  (ctx->switch_state == SwitchStateAlwaysOn)
#define light_is_automatic(ctx)   (ctx->switch_state == SwitchStateAutomatic)

#define toggle_switch_if_needed(ctx) DO( \
    if (ctx->notification_value & NOTIFY_BUTTON_TOUCH) { \
      ctx->switch_state = light_is_allways_on((ctx)) ? SwitchStateAutomatic : SwitchStateAlwaysOn; \
    } \
)

typedef struct {
  uint8_t switch_state;
  int distance;
  uint32_t light;
  uint32_t notification_value;
} LighterContext;

LighterContext ligth_ctx = {
  .switch_state = SwitchStateAutomatic,
  .notification_value = 0,
};

xQueueHandle display_queue = NULL;


static __inline TickType_t time_to_wait(LighterContext *ctx) {
  return light_is_allways_on(ctx) ? 3000 : 1000;
}

static __inline BaseType_t wait_event(LighterContext *ctx) {
  TickType_t timeout = time_to_wait(ctx);
  return xTaskNotifyWait(UINT32_MAX, 0, &ctx->notification_value, timeout);
}

static __inline void trigger_usonic_if_needed(LighterContext *ctx) {
  if ((ctx->notification_value & (NOTIFY_USONIC_VALUE | NOTIFY_USONIC_ERROR)) == 0) {
    usonic_value_triger_async();
  }
}

#if USONIC_TIMER_INTERRUPTS
#define get_distance(distance, ctx)        DO( \
  if (ctx->notification_value & NOTIFY_USONIC_VALUE) { \
    distance = usonic_mm_from_us(ctx->notification_value & NOTIFY_VALUE_MASK); \
  } else if (ctx->notification_value & NOTIFY_USONIC_ERROR) { \
    distance = -1; \
  } \
 ) //DBG_PRINTF("Echo end %i mm", ctx->distance);

#else
#define get_distance(distance, ctx)
#endif //USONIC_TIMER_INTERRUPTS



static void measure_task(LighterContext *ctx) {
  int distance = 0;
  uint32_t light = 0;
  BaseType_t has_event = pdFALSE;
  gpio_set_bottom_half_handler(xTaskGetCurrentTaskHandle());
  usonic_set_bottom_half_handler(xTaskGetCurrentTaskHandle());
  for( ;; ) {
#if USONIC_INTERRRUPTS
   trigger_usonic_if_needed(ctx);
#else
    distance = usonic_get_value();
    DBG_PRINTF("Echo end %i mm", ctx->distance);
#endif
    light = lsensor_get_value();
    ctx->distance = distance;
    ctx->light = light;
    if (has_event == pdTRUE) {
      toggle_switch_if_needed(ctx);
      get_distance(distance, ctx);
    }
    xQueueSendToBack(display_queue, ctx, 0);
    has_event = wait_event(ctx);
  }
}

static __inline uint32_t led_level_for_light(uint32_t light) {
  // 2500 and below - darkness
  // 3500 and more - daylight.
  // This values might have been re-calibrated for different photo sensors.
  const uint32_t divisor = 4;
  const uint32_t levelMax = 2500 + 255 * divisor;
  uint32_t level = 0;
  if (light <= 2500) {
    level = 255;
  } else if (light >= levelMax) {
    level = 0;
  } else {
    light -= 2500;
    level = 255 - light / divisor;
  }
    DBG_PRINTF("New light level %u", level);
  return level;
}

static void display_task(xQueueHandle queue) {
  uint8_t buff[16];
  LighterContext ctx_buf;
  LighterContext *ctx = &ctx_buf;
  for( ;; ) {
    // Block indefinitely until data is ready.
    if (xQueueReceive(queue, ctx, portMAX_DELAY) != pdTRUE) {
      continue;
    }
    if (light_is_automatic(ctx)) {
      if (ctx->distance >= 0) {
        if (ctx->light != 0) {
          // There is no light 0. Most likely 0 means ADC error.
          nokia5510_set_brightness(led_level_for_light(ctx->light));
        }
      } else {
        nokia5510_set_brightness(0);
      }
    } else {
      nokia5510_set_brightness(255);
    }
    nokia5510_clear();
    snprintf(buff, 16, "Light %u", ctx->light);
    nokia5510_put_line(buff);
    if (ctx->distance >= 0) {
      snprintf(buff, 16, "Distance %i", ctx->distance);
      nokia5510_put_line(buff);
    } else {
      nokia5510_put_line_inverse("usonic error");
    }
    snprintf(buff, 16, "MODE: %s", light_is_allways_on(ctx) ? "on" : "auto");
    nokia5510_put_line(buff);
  }
}


int main() {
  usonic_setup();
  test_usonic();
  pbutton_setup();
  lsensor_setup();
  nokia5510_init();

  display_queue = xQueueCreate(2, sizeof( LighterContext ));

  xTaskCreate((TaskFunction_t)measure_task, "measure", 240, (void *)(&ligth_ctx), HIGH_PRIORITY, NULL);
  xTaskCreate((TaskFunction_t)display_task, "display", 640, (void *)display_queue, LOW_PRIORITY, NULL);
  vTaskStartScheduler();
  for( ;; ) { }
  return 0;
}

