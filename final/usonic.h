/*
  HC-SR04 ultrasonic sonar driver for LPC1769 board.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#ifndef __USONIC_INCLUDED__
#define __USONIC_INCLUDED__


// p0.7 - Trig
// p0.24 - Echo
// UM10360.pdf, p117.
#define USONIC_TRIG_PIN       7
#define USONIC_ECHO_PIN       24
#define PINSEL0_USONIC_MASK   BITS_14_15_ZERO   // p0.7  as GPIO
#define PINSEL1_USONIC_MASK   BITS_16_17_ZERO   // p0.24 reset
#define PINSEL1_USONIC_SET    BITS_16_17_ONE    // p0.24 as CAP3.1
#define USONIC_TRIG_BIT       (1 << USONIC_TRIG_PIN)
#define USONIC_ECHO_BIT       (1 << USONIC_ECHO_PIN)
#define USONIC_ECHO_LOW       (~USONIC_ECHO_BIT)

#define USONIC_TIMER_PORT     LPC_TIM3

#if USONIC_INTERRRUPTS
void usonic_set_bottom_half_handler(TaskHandle_t new_bottom_half);
#else
#define usonic_set_bottom_half_handler(_X_)
#endif

void usonic_setup();

int usonic_get_value();

__inline static uint32_t usonic_mm_from_us(uint32_t us) {
  return (us * 50) / 291;
}

#if USONIC_INTERRRUPTS


void usonic_value_triger_async();


void usonic_echo_start_FromISR();

void usonic_echo_end_FromISR();
#endif

#endif //__USONIC_INCLUDED__
