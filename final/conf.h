/*
  Config file.
  This file is part of Real-time Embedded Systems Programming Final project.
  Copyright (C) 2016 Eugene Hermann.
  This guy for real is a engineering slave without any rights, so nothing to reserve :(.
  Feel free to violate.

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License (version 2) as published by the
  Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

  Don't be evil.
*/

#include <stdlib.h>
#include <stdio.h>

#define HIGH_PRIORITY 2
#define LOW_PRIORITY 1

#define USE_DBG_PRINT 0
#define TEST_USONIC   0

#define USONIC_INTERRRUPTS 1
#define USONIC_GPIO_INTERRUPTS  (0 & USONIC_INTERRRUPTS)
#define USONIC_TIMER_INTERRUPTS (1 & USONIC_INTERRRUPTS)

#define PCD_8544_SSP_EMULATION 0


#define DO(__something__) do { __something__; } while (0)

#if USE_DBG_PRINT
#define DBG_PRINTF(__fmt__,  args...)  DO( printf("%s: "__fmt__"\n", __func__, args) )
#else
#define DBG_PRINTF(__fmt__,  args...)
#endif

#if TEST_USONIC
void test_usonic();
#else
#define test_usonic()
#endif

// Readable bit names
#define BIT_0_ONE 1
#define BIT_1_ONE (1 << 1)
#define BIT_2_ONE (1 << 2)
#define BIT_3_ONE (1 << 3)
#define BIT_4_ONE (1 << 4)
#define BIT_5_ONE (1 << 5)
#define BIT_6_ONE (1 << 6)

#define BIT_7_ONE (1 << 7)
#define BIT_7_ZERO (~BIT_7_ONE)
#define BIT_8_ONE (1 << 8)
#define BIT_9_ONE (1 << 9)

#define BIT_23_ONE (1 << 23)



#define BITS_16_17_ONE  (0x03 << 16)
#define BITS_16_17_ZERO  (~BITS_16_17_ONE)
#define BITS_14_15_ZERO (~(0x03 << 14))
#define BITS_14_17_ZERO (~(0x0f << 14))
#define BITS_12_13_ZERO (~(0x03 << 12))

#define PCONP_PCPWM1          BIT_6_ONE
#define PCLKSEL0_PCLK_PWM1    BITS_12_13_ZERO

// Task notification mask
#define NOTIFY_BUTTON_TOUCH   (1 << 16)
#define NOTIFY_USONIC_VALUE   (1 << 17)
#define NOTIFY_VALUE_MASK     0xffff
#define NOTIFY_USONIC_ERROR   (1 << 18)
